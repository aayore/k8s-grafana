# k8s-grafana

This repo is intended as a small functional example of using Grafana in Kubernetes (K8s).

## Usage

The services contained in this repository are designed to use the AWS LoadBalancer construct.  Therefore this may take some work to run outside of AWS (and/or EKS).  All resources can be brought up using `kubectl apply -f <resource_spec>`

## Dependency Order

0. The Kubernetes cluster must have an `ebs-gp2` storage class.
1. The Kubernetes cluster must have a secret named `grafana` with an admin stored (base64-encoded) at `.data.password`
    1. A cluster admin can verify this with `kubectl get secret grafana --output=jsonpath={.data.password} | base64 --decode`
    2. See `secret-example.yaml`
2. Volume for persistent data
    1. `grafana-pvc.yaml`
3. Grafana deployment
    1. `grafana-deploy.yaml`
4. Grafana service
    1. `grafana-service.yaml`
5. Dashboard import
    1. Configure the `MapquestOpsCloudWatch` data source in Grafana.
    2. `grafana_import.sh`
