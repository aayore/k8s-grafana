#!/usr/bin/env python

import boto3

region = 'us-west-2'
hosted_zones = {
    'cloud.mapquest.com': 'Z1A7MZMQWUPMUU',
    'getcrashdetection.com': 'Z30JWYF4PC3YBV',
    'ownyourdrive.com': 'Z20H1CWXP0HTBT',
    'understandyourdrive.com': 'Z18L1CYYGLJT8K',
    'mapkin.co': 'Z3TGDBKBY6QTVU',
    'kismetapp.io': 'ZHCOJ5IZ4B5KA',
    'mapquest.io': 'Z24OM53H597SKM',
    'mapq.st': 'Z5DH60E5CM99D',
    'solutions.mapquest.com': 'Z360WXGWCVH3CW'
}

# This is dependent on the service defined in grafana-service.yaml
elb_dns_name = 'ae103b91b6a9811e8988e02392034bcb-597820697.us-west-2.elb.amazonaws.com'

service_dns_name = 'grafana.cloud.mapquest.com'

route53 = boto3.client('route53',region_name=region)

change = {
  "Comment": "Modified via dns.py",
  "Changes": [
    {
      "Action": "UPSERT",
      "ResourceRecordSet": {
        "Name": service_dns_name,
        "Type": "A",
        "AliasTarget": {
          "HostedZoneId": hosted_zones['cloud.mapquest.com'],
          "DNSName": elb_dns_name,
          "EvaluateTargetHealth": False
        }
      }
    }
  ]
}

response = route53.change_resource_record_sets(HostedZoneId=hosted_zones['cloud.mapquest.com'],ChangeBatch=change)

print(response['ChangeInfo'])
